#!/usr/bin/env bash
set -e

USERNAME="imghub"
REPOSITORY="imghub"
GITHOST="gitlab.com"
BRANCH="master"

PRE_URL="https://"${GITHOST}/${USERNAME}/${REPOSITORY}/raw/${BRANCH}

function usage(){
        echo "Usage: `basename $0` [options] filename"
}

function get_link(){
    echo -e "\ngenerate link\n"
    #echo ${PRE_URL}

    if [ -z $1 ];then
        echo "please input image file name!!!"
        exit 1
    else
        for var in $@;do
            echo -e "!["$(basename ${var})"]("${PRE_URL}/$(basename ${var})")\n"
         done
    fi
}

function upload_img(){
    git add $@
    git status
    read -s -n1 -p  "Are you sure you want to continue (yes/no)?"
    git commit -m "add "$@
    git push
}

while getopts ':u:g:' opt
do
    case $opt in
        u)
            u=${OPTARG}
            upload_img $u
        ;;
        g)
            g=${OPTARG}
            get_link $g
        ;;
        *)
            usage
            exit 1
        ;;
    esac
done

shift $((OPTIND-1))
if [ -z "$g" ] && [ -z "$u" ]; then
    usage
fi
